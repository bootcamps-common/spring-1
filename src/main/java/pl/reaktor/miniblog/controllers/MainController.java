package pl.reaktor.miniblog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.reaktor.miniblog.services.UserSessionService;

@Controller
public class MainController {

    private UserSessionService userSessionService;

    @Autowired
    public MainController(UserSessionService userSessionService) {
        this.userSessionService = userSessionService;
    }

    @RequestMapping("/")
    public String indexPage(Model model, Authentication auth){
        if(auth != null){
            UserDetails principal = (UserDetails) auth.getPrincipal();
            System.out.println(principal.getUsername() + " :: " + principal.getAuthorities());
            model.addAttribute("principal", principal);
        }
        model.addAttribute("name", "Iwan");
        model.addAttribute("userLogged", userSessionService.isUserLogged());
        return "index";
    }

    @RequestMapping(value = "/hello")
    public String helloPage(Model model, @RequestParam(value = "name", required = false) String paramName){
        if(paramName != null){
            model.addAttribute("text", "Hi, " + paramName);
            model.addAttribute("name", paramName);
        } else {
            model.addAttribute("text", "Hi");
        }
        String text = paramName != null ?  paramName : "brak";
        model.addAttribute("href", "/");
        return "hello-page";
    }

    @RequestMapping(value = "/hello/{name}/{age}")
    public String helloPageWithName(Model model, @PathVariable String name, @PathVariable (value = "age") int paramAge,
                                    @RequestParam (required = false) String gender,
                                    @RequestHeader ("user-agent") String userAgent){
        if(gender != null){
            model.addAttribute("text", "Hi, " + name + ", your age: " + paramAge + ", gender: " + gender);

        } else {
            model.addAttribute("text", "Hi, " + name + ", your age: " + paramAge);
        }
        model.addAttribute("userAgent", userAgent);
        model.addAttribute("href", "/");
        return "hello-page";
    }

}
