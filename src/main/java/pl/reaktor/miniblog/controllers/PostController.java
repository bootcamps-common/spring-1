package pl.reaktor.miniblog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.reaktor.miniblog.model.entities.Post;
import pl.reaktor.miniblog.repositories.PostRepository;
import pl.reaktor.miniblog.services.PostService;

import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

@Controller
public class PostController {

    private PostRepository postRepository;
    private PostService postService;

    @Autowired
    public PostController(PostRepository postRepository, PostService postService) {
        this.postRepository = postRepository;
        this.postService = postService;
    }

    @RequestMapping(value = "/posts")
    public String showAllPosts(Model model){
        List<Post> posts = postRepository.findAll();
        model.addAttribute("posts", posts);
        return "posts2";
    }

    @RequestMapping(value = "/post/add", method = RequestMethod.GET)
    public String addPostPage(){
        return "post_add";
    }

    @RequestMapping(value = "/post/add", method = RequestMethod.POST)
    public String addPostAction(@RequestParam String title, @RequestParam String content){
        System.out.println("Params: " + title + " :: " + content);

        Post post = new Post();
        post.setTitle(title);
        post.setContent(content);
        post.setAuthor("Jarek");

        postRepository.save(post);

        return "post_add";
    }

    @GetMapping("/post/{postId}")
    public String showPost(@PathVariable Long postId, Model model){
        Optional<Post> postOptional = postRepository.findById(postId);

//        if (postOptional.isPresent()){
//            model.addAttribute("post", postOptional.get());
//        }

        postOptional.ifPresent(getPostConsumer(model));

//        Post post = postOptional.get();
//        postRepository.delete(post);


        //Post one = postRepository.getOne(postId);
        return "post2";
    }

    //th:action="@{/post/{postId}/addComment(postId=${post.id})}"
    @PostMapping("/post/{postId}/addComment")
    public String addCommentByPath(@PathVariable Long postId, @RequestParam String author, @RequestParam String commentBody){
        System.out.println("Params: " + author + ", " + commentBody);
        postService.addCommentToPost(postId, author, commentBody);
        return "redirect:/post/" + postId;
    }

    //th:action="@{/post/addComment}" + input type="hidden"
    @PostMapping("/post/addComment")
    public String addCommentByParam(@RequestParam Long postId, @RequestParam String author, @RequestParam String commentBody){
        System.out.println("Params: " + author + ", " + commentBody);
        boolean commentAdded = postService.addCommentToPost(postId, author, commentBody);
        if(!commentAdded){
            return "redirect:/post/" + postId + "?msg=error" ;
        }
        return "redirect:/post/" + postId;
    }

    private Consumer<Post> getPostConsumer(Model model) {
        return abc -> {
                System.out.println(abc);
                abc.getAuthor();
                model.addAttribute("post", abc);
            };
    }


}
