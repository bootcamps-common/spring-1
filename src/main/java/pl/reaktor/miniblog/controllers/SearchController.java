package pl.reaktor.miniblog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.reaktor.miniblog.model.entities.Post;
import pl.reaktor.miniblog.repositories.PostRepository;

import java.util.List;

@Controller
public class SearchController {

    private PostRepository postRepository;

    @Autowired
    public SearchController(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

    @GetMapping("/search")
    public String search(@RequestParam (value = "q") String query, Model model){
        //List<Post> posts = postRepository.findAllByTitleContains(query);
        //List<Post> posts = postRepository.findAllByTitleContainsOrderByIdDesc(query);
        List<Post> posts = postRepository.findPostsWithPhrase(query);
        model.addAttribute("posts", posts);
        model.addAttribute("simplePosts", postRepository.findPostsWithPhraseProjection(query));
        return "posts2";
    }
}
