package pl.reaktor.miniblog.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.reaktor.miniblog.model.dtos.UserDto;
import pl.reaktor.miniblog.model.entities.User;
import pl.reaktor.miniblog.model.forms.RegisterForm;
import pl.reaktor.miniblog.services.UserService;
import pl.reaktor.miniblog.services.UserSessionService;

import javax.validation.Valid;
import java.util.Optional;

@Controller
public class UserController {

    private UserService userService;
    private UserSessionService userSessionService;

    @Autowired
    public UserController(UserService userService, UserSessionService userSessionService) {
        this.userService = userService;
        this.userSessionService = userSessionService;
    }

    @GetMapping("/register")
    public String registerPage(Model model){
        model.addAttribute("registerForm", new RegisterForm());
        return "register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute @Valid RegisterForm registerForm, BindingResult bindingResult, Model model){
//        if(registerForm.getFirstname().length() < 3){
//            bindingResult.rejectValue("firstname", "123", "Złe imie");
//        }
        if (bindingResult.hasErrors()){
            return "register";
        }

        userService.createUser(registerForm);
        //System.out.println(registerForm);

        return "redirect:/";

    }

    @GetMapping("/user/login")
    public String login(){
        userSessionService.login();
        return "redirect:/";
    }

    @GetMapping("/user/logout")
    public String logout(){
        userSessionService.logout();
        return "redirect:/";
    }

    @GetMapping("/login")
    public String loginPage(){
        return "login";
    }

    @GetMapping("/user/{userId}")
    public String showSingleUser(@PathVariable String userId, Model model){

        Optional<UserDto> optionalUser = userService.findUserById(Long.valueOf(userId));

        if(optionalUser.isPresent()){
            model.addAttribute("user", optionalUser.get());
        } else {
            model.addAttribute("user", new UserDto());
        }

        return "user";
    }

    @GetMapping("/user") //http://localhos:8080/user?userId=1
    public String showSingleUserByParam(@RequestParam String userId, Model model){
        return "redirect:/user/" + userId;
    }

}
