package pl.reaktor.miniblog.model.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SimplePostDto {
    private Long id;
    private String title;
}
