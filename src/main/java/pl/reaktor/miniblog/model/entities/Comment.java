package pl.reaktor.miniblog.model.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@ToString (exclude = "post")
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date added = new Date();
    private String author;
    private String commentBody;

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;
}
