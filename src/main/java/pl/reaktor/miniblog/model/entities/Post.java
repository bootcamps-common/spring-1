package pl.reaktor.miniblog.model.entities;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@ToString
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "post_title")
    private String title;

    @Column(length = 4000)
    private String content;

    private Date added = new Date();

    private String author;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "post")
    private List<Comment> comments = new ArrayList<>();

    public void addComment(Comment comment){
        comment.setPost(this);
        comments.add(comment);
    }
}
