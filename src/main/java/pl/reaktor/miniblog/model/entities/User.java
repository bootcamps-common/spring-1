package pl.reaktor.miniblog.model.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Setter @Getter
//@ToString
@Table(name = "user")
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public User(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    private String email;
    private String password;
    private String firstname;
    private String lastname;

    private Date added = new Date();

    @ManyToMany
    @JoinTable(name = "user_role")
    private Set<Role> roles;

    @Column(name = "enabled")
    private boolean active;

    @Override
    public String toString() {
        return "User{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }
}
