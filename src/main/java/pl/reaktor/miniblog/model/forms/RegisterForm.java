package pl.reaktor.miniblog.model.forms;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.Date;

@Setter @Getter
@ToString(exclude = "password")
@NoArgsConstructor
public class RegisterForm {

    @Pattern(regexp = "[\\p{L}0-9]+@[a-z0-9]+\\.[a-z]{2,3}")
    //@Email
    private String email;
    @NotBlank
    private String password;
    @Size(min = 6, max = 20, message = "Imię nieprawidłowe. Podaj min {min} znaków a max {max}. Podałeś ${validatedValue}")
    private String firstname;
    @NotBlank
    private String lastname;

    public RegisterForm(String firstname) {
        this.firstname = firstname;
    }
}
