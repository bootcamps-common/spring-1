package pl.reaktor.miniblog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.reaktor.miniblog.model.entities.Comment;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
}
