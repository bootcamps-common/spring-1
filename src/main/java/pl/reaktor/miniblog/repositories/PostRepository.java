package pl.reaktor.miniblog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.reaktor.miniblog.model.dtos.SimplePostDto;
import pl.reaktor.miniblog.model.entities.Post;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    List<Post> findAllByTitleContains(String title);

    List<Post> findAllByTitleContainsOrderByIdDesc(String title);

    List<Post> findAllByTitleContainsAndContentContains(String likeTitle, String likeContent);
    List<Post> findAllByTitleContainsOrContentContains(String likeTitle, String likeContent);

    @Query("SELECT p FROM Post p WHERE p.title like %:phrase% AND p.content like %:phrase% ")
    List<Post> findPostsWithPhrase(@Param("phrase") String phrase);

    @Query("SELECT new pl.reaktor.miniblog.model.dtos.SimplePostDto(p.id, p.title) FROM Post p WHERE p.title like %:phrase% AND p.content like %:phrase% ")
    List<SimplePostDto> findPostsWithPhraseProjection(@Param("phrase") String phrase);

}
