package pl.reaktor.miniblog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.reaktor.miniblog.model.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
