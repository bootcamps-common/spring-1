package pl.reaktor.miniblog.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.reaktor.miniblog.model.entities.Comment;
import pl.reaktor.miniblog.model.entities.Post;
import pl.reaktor.miniblog.repositories.CommentRepository;
import pl.reaktor.miniblog.repositories.PostRepository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    private CommentRepository commentRepository;
    private PostRepository postRepository;

    @Autowired
    public PostService(CommentRepository commentRepository, PostRepository postRepository) {
        this.commentRepository = commentRepository;
        this.postRepository = postRepository;
    }

    public boolean addCommentToPost(Long postId, String author, String commentBody) {

        Optional<Post> optionalPost = postRepository.findById(postId);


        if(!optionalPost.isPresent()){
            System.out.println("Post not found: " + postId);
            return false;
        }

        Comment comment = new Comment();
        comment.setAuthor(author);
        comment.setCommentBody(commentBody);
        //comment.setPost(optionalPost.get());

        //comment.setPost(optionalPost.get());
        //Comment savedComment = commentRepository.save(comment);

        Post post = optionalPost.get();
        System.out.println("Before post.getComments()");
//        List<Comment> postComments = post.getComments();
//        System.out.println("After post.getComments()");
//        System.out.println("Comments: " + postComments);
//        postComments.add(comment);
        post.addComment(comment);
        postRepository.save(post);


        return true;
    }

}
