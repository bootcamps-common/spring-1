package pl.reaktor.miniblog.services;

import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.reaktor.miniblog.controllers.MainController;
import pl.reaktor.miniblog.model.dtos.UserDto;
import pl.reaktor.miniblog.model.entities.User;
import pl.reaktor.miniblog.model.forms.RegisterForm;
import pl.reaktor.miniblog.repositories.PostRepository;
import pl.reaktor.miniblog.repositories.UserRepository;

import java.util.Map;
import java.util.Optional;

@Service
//@Scope("prototype")
public class UserService {
    private UserRepository userRepository;

    private Map<String, Object> cache;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(RegisterForm registerForm){
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

        User user = new User();
        user.setEmail(registerForm.getEmail());
        user.setFirstname(registerForm.getFirstname());
        user.setLastname(registerForm.getLastname());
        user.setPassword(bCryptPasswordEncoder.encode(registerForm.getPassword()));

        User savedUser = userRepository.save(user);
        return savedUser;
    }

    public Optional<UserDto> findUserById(Long userId){
        Optional<User> userOptional = userRepository.findById(userId);

        if(!userOptional.isPresent()){
            return Optional.empty();
        }
        User user = userOptional.get();

        UserDto userDto = new UserDto(user.getId(), user.getEmail(), user.getFirstname(), user.getLastname());

        return Optional.of(userDto);
    }
}
