package pl.reaktor.miniblog.services;

import lombok.Getter;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserSessionService {
    @Getter
    private boolean userLogged;

    public void login(){
        this.userLogged = true;
    }

    public void logout(){
        this.userLogged = false;
    }

}
