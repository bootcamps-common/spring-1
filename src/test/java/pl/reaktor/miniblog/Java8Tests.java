package pl.reaktor.miniblog;

import org.junit.Assert;
import org.junit.Test;
import pl.reaktor.miniblog.model.entities.User;

import java.util.*;
import java.util.stream.Collectors;

public class Java8Tests {

    @Test
    public void firstTest(){
        System.out.println("Testujemy!!! - " + (2 + 2));
        //przygotowanie danych wejsciowych i warunków brzegowych testu
        int a = 2;
        int b = 3;

        //faktyczne testowanie logiki
        int wynik = calculate(a, b);

        //sprawdzanie wyników
        Assert.assertEquals(5, wynik);
    }

    private int calculate(int x, int y){
        return x + y;
    }

    @Test
    public void sorting(){
        List<User> users  = new ArrayList<>();
        users.add(new User("Jan", "Nowak"));
        users.add(new User("Kamil", "Nowak"));
        users.add(new User("Jan", "Adamczewski"));

        System.out.println("Before: " + users);

        Collections.sort(users, (o1, o2) -> o1.getFirstname().compareTo(o2.getFirstname()));
        System.out.println("After: " + users);
    }

    @Test
    public void sortingWithStream(){
        List<User> users  = new ArrayList<>();
        users.add(new User("Jan", "Nowak"));
        users.add(new User("Kamil", "Nowak"));
        users.add(new User("Jan", "Adamczewski"));

        System.out.println("Before: " + users);

        Set<User> userSet = users.stream()
                .filter(u -> u.getFirstname().equals("Jan"))
                .map(u -> new User(u.getLastname(), u.getFirstname()))
                .collect(Collectors.toSet());

        System.out.println("After: " + users);
        System.out.println("After: " + userSet);
    }

    @Test
    public void sortingWithStream2(){
        List<User> users  = new ArrayList<>();
        users.add(new User("Jan", "Nowak"));
        users.add(new User("Kamil", "Nowak"));
        users.add(new User("Jan", "Adamczewski"));

        System.out.println("Before: " + users);

        Set<String> jan = users.stream()
                .filter(u -> u.getFirstname().equals("Jan"))
                .map(u -> new User(u.getLastname(), u.getFirstname()))
                .map(u -> u.getFirstname())
                .collect(Collectors.toSet());

        System.out.println("After: " + users);
        System.out.println("After: " + jan);
    }
}
